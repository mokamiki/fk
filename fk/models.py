from django.db import models


class Issue(models.Model):
    registration_number = models.CharField(max_length=30)
    subject = models.CharField(max_length=50)
    user_full_name = models.CharField(max_length=50)
    som_date = models.DateField()
    date = models.DateField()
    status = models.DecimalField(max_digits=1, decimal_places=0, default=1)

    def __str__(self):
        return self.registration_number

    class Meta:
        permissions = (
            ("can_add_new_issue", "Add new issue"),
        )
