from django.urls import path

from . import views
from fk.views import IssueCreateView
from fk.models import Issue

urlpatterns = [
    path('', views.index, name='index'),
    path('create/', IssueCreateView.as_view(model=Issue, success_url='/success/')),
    path('success/', views.success, name='success'),
    path('opened/', views.index, name='index'),
    path('suspended/', views.suspended, name='suspended'),
    path('petition/', views.petition, name='petition'),
    path('department/', views.department, name='department'),
    path('late/', views.late, name='late'),
    path('closed/', views.closed, name='closed'),
    path('logout/', views.logout_view),
]
