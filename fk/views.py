from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from django.views.generic import CreateView
from .models import Issue


@login_required(login_url='login/')
def index(request):
    return render(request, 'fk/opened.html', {'issues': Issue.objects.all()})


@login_required(login_url='login/')
def late(request):
    return render(request, 'fk/late.html')


@login_required(login_url='login/')
def suspended(request):
    return render(request, 'fk/suspended.html')


@login_required(login_url='login/')
def petition(request):
    return render(request, 'fk/petition.html')


@login_required(login_url='login/')
def department(request):
    return render(request, 'fk/department.html')


@login_required(login_url='login/')
def closed(request):
    return render(request, 'fk/closed.html')


@login_required(login_url='login/')
def success(request):
    return render(request, 'fk/success.html')


def logout_view(request):
    logout(request)
    return render(request, 'registration/logged_out.html')


class IssueCreateView(CreateView):
    model = Issue
    template_name = "fk/create.html"
    exclude = ('status')
    fields = ('registration_number', 'subject', 'user_full_name', 'som_date', 'date')

